# VUE-MMCC 

> Multi Modal Chatbot Creator


<div class="bordered green">

## 🤩 - HYPERMEDIA Assignment


For instructions on the (optional) Hypermedia course assignment, click [here](https://gitlab.com/i3lab/mmcc/frontend/mmcc-vue/-/tree/master/docs/hypermedia-assignment.md)

</div>

<div class="bordered green">

## 🧐 - HYPERMEDIA JSON


For instructions on how to structure the JSON, click [here](https://gitlab.com/i3lab/mmcc/frontend/mmcc-vue/-/tree/master/docs/hypermedia-json.md)

</div>
<div class="bordered green">

## 🚀 - HYPERMEDIA Step-by-step installation

To learn how to install vue-mmcc within your Hypermedia project, click [here](https://gitlab.com/i3lab/mmcc/frontend/mmcc-vue/-/tree/master/docs/hypermedia-step-by-step.md)


</div>
<div class="bordered">

## 💻 - Vue MMCC Advanced

> <span class="underline">! Not required for the Hypermedia course !</span>

To know more about MMCC and the other features it offers, click [here](https://gitlab.com/i3lab/mmcc/frontend/mmcc-vue/-/tree/master/docs/advanced.md)

</div>