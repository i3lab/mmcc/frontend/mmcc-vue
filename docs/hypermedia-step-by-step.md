# MMCC Step by Step

## 0. Pull the project shown in class

This repo contains many pieces of code we are going to re-use in your projects!

Link to the repo: [https://gitlab.com/hypermedia2020-21/hands-on-3.git](https://gitlab.com/hypermedia2020-21/hands-on-3.git)

Terminal command to execute:

```bash
git clone https://gitlab.com/hypermedia2020-21/hands-on-3.git
```

## 1. Upload the process Json on the framework website

1. Log in to https://mmcc.i3lab.group and upload your configuration.
2. Once it has been correctly uploaded, you will receive a configuration id we will use in the project

If you don't know how to structure the JSON, you can look into the course material.

## 2. Install mmcc dependency

Open your project and install mmcc dependency

To do that, run the following operation from the terminale

```bash
npm install git+https://gitlab.com/i3lab/mmcc/frontend/mmcc-vue
```

## 3. Create an environment variable for the configuration id

1. Open nuxt.config.js file
2. Register a new environment variable, called `configurationId` , containing the configuration id you obtained uploading the process JSON in the platform
3. To do that, add a new field called env to the default object:

    ```jsx
    env: {
        configurationId: 'YOUR CONFIG ID',
      },
    ```

## 4. Create mmcc plugin

Remember: plugins are code that is executed before instantiating the root Vue.js Application. With our plugin, we are saying to the frontend to create a connection with the framework running in the backend.

For more information on plugins visit: [https://nuxtjs.org/docs/2.x/directory-structure/plugins](https://nuxtjs.org/docs/2.x/directory-structure/plugins)

1. Create a file `mmcc.js` in the plugins folder of your project
2. Open the file and insert the following code

    ```jsx
    import Vue from 'vue'
    import OnClick from 'mmcc/OnClick.vue'

    Vue.component('mmcc-on-click', OnClick)
    ```

    As shown in the hands-on-3 repository.

3. Globally register the component:
    1. Open nuxt.config.js file
    2. Insert the path of the file you have gust created in the plugins array

        ```jsx
        plugins: ['~/plugins/mmcc.js'],
        ```

At this point, you should have env and plugins fields in nuxt.config.js that contain (at least) the same information contained the ones shown in class.

## 5. Create a mixin to manage the chat

We use a mixin to separate the logic of the chat from the default layout, such that it can be used in any page of the website.

1. Inside mixins folder of your project, create a file named mmcc-mixin.js
2. In the file, insert the following code:

    ```jsx
    export default {
      data() {
        const list = []
        const configurationId = process.env.configurationId
        return { list, configurationId }
      },
      watch: {
        '$store.state.messages'() {
          this.list = this.$store.state.messages
        },
      },
      mounted() {
        const { WebSocketEventBus } = require('mmcc/WebSocketEventBus')
        // Connection config
        const data = {
          configurationId: process.env.configurationId,
          interaction: localStorage.getItem('mmcc-interaction') || null,
        }
        // Emitting the connect event to initialize the communication
        WebSocketEventBus.$emit('connect', data)

        // Adding the default event listener for messages
        WebSocketEventBus.onMessage((message) => {
          if (message.utterance) {
            this.$store.commit('addMessage', {
              sender: true,
              content: message.utterance,
            })
          }
          if (message.payload) {
            if (message.payload.guide) {
              this.$router.push(message.payload.guide)
            }
          }
        })
      },
    }
    ```
The `interaction` variable allows the communication to survive in case of interruption, at different levels of persistence.
It is sent from the backend in the *first message of communication*.
You can use it as you want! In the example is got from `localStorage`.

Depending on your need, you can save it in different places:

|Storage|Notes|
|---|---|
|`data()`|Data is saved in RAM, so it survives only from sudden interruptions caused by WebSocket connection errors, *not involving the reload of the tab.*|
|`sessionStorage`|All the above plus reload of the tab. Note that different tabs will belong to different storage objects, thus data is not shared.|
|`localStorage`|All the above plus the browser quitting. This is the most persistent level. IMPORTANT: If you want to delete this data (allowing for a brand new Chatbot interaction), you have to do it manually into the Developer tools or call `localStorage.clear()` method somewhere in your app.|

## 6. Create chat store

Inside you store, add a the code required to manage the chat:

```jsx
import Vue from 'vue'

export const state = () => {
  return {
    messages: [],
  }
}

export const mutations = {
  addMessage(state, message) {
    const messages = state.messages
    messages.push(message)
    Vue.set(state, 'messages', messages)
  },
}
```

## 7. Import the chat

Create a new chat component, as the one shown in class:

[https://gitlab.com/hypermedia2020-21/hands-on-3/-/blob/master/components/Chat.vue](https://gitlab.com/hypermedia2020-21/hands-on-3/-/blob/master/components/Chat.vue)

You can customize the chat as much as you want (functionalities, animations, aspect, ...), paying attention to keep the component and its store coherent! 

## 8. Register the chat and the mixin into the `default`

The `default` layout adds a layer in Vue through which you can exchange useful information.
In this case the created `mixin` containing the messages list.

1. Open the default layout
2. Import the chat and the mixin, and register them:

    ```jsx
    <script>
    import Chat from '~/components/Chat'
    import MMCCMixin from '~/mixins/mmcc-mixin'
    export default {
      components: {
        Chat,
      },
      mixins: [MMCCMixin],
    }
    </script>
    ```

3. Insert the chat in the template of default:

    ```jsx
    <template>
      <div>
        <Nuxt />
        <chat :chat-list="list" />
      </div>
    </template>
    ```

## 9. Have fun experimenting it!
