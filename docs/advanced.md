# MMCC-VUE


This package adds a layer of connection for process-based chatbot frameworks.
It allows to decouple the frontend ui rendering from the connection management handled by WebSocket or REST API.
It is almost completely compatible with former web applications without too much integration effort.

To work with it, you need to set up a backend chatbot service before.


## Install

```bash
npm install --save git+https://gitlab.com/i3lab/mmcc/frontend/mmcc-vue
```

## Usage

Firstly, you need to wrap your all your application components that need a network connection layer with the components.
```jsx
<script>
import OnClick from './vue-mmcc/OnClick';
import OnSubmit from './vue-mmcc/OnSubmit';

export default {
  name: 'App',
  components: {
    OnClick,
    OnSubmit,
  },
  data: function () {
    return {
      stopPropagation: true,
      onOpen: () => console.log('test: onOpen prop!'),
    }
  }
}
</script>
```

Then, you can wrap your components with the event catchers for each component type.

```vue
<template>
  <div id="app">
    <img alt="Vue logo" src="./assets/logo.png">
    <div id="container">
      <h2>Vue.js WebSocket handler</h2>
      <on-click configuration-id="12157482-c72b-45f0-a1ee-221bbcecad85" :on-open="onOpen">
        <button onclick="alert('To be shown')">Stop Propagation: false</button>
      </on-click>
      <br>
      <on-click configuration-id="2defad22-27df-4754-9701-7d0d11073d4c" :stop-propagation="stopPropagation">
        <button onclick="alert('Not to be shown')">Stop Propagation: true</button>
      </on-click>
      <br>
      <on-submit type="utterance">
        <form>
          <input type="text">
          <input type="submit">
        </form>
      </on-submit>
    </div>
  </div>
</template>
```

## Components

<!-- ### `NetworkManager`
It is used to manage the connection with the backend service and handle the callbacks.

#### Props

| Prop      | Type      | Default     | Note                                                                                                                           |
| --------- | --------- | ----------- | ------------------------------------------------------------------------------------------------------------------------------ |
| `url`     | `String`  | `undefined` | (Required) The url of the connection. It supports WebSocket protocol. In future it will handle HTTP connections with REST API. |
| `uid`     | `String`  | `undefined` | (Required) The unique identifier of the connection Must be pre-shared before connecting.                                       |
| `useRest` | `boolean` | `false`     | Not implemented yet.                                                                                                           |
 -->
<!-- ### Events
| Event       | Returns  | Note                                                                                               |
| ----------- | -------- | -------------------------------------------------------------------------------------------------- |
| `onMessage` | `string` | A callback function which triggers the receive event. This is used to handle the received messages |
| `onOpen`    | `Event`  | A callback function which triggers the open event.                                                 |
| `onClose`   | `Event`  | A callback function which triggers the close event.                                                |
| `onError`   | `Event`  | A callback function which triggers the error event.                                                | | -->

### `OnClick`
The OnClick component handles all click events passed through its children.

#### Props

| Prop              | Type       | Default      | Note                                                                                                |
| ----------------- | ---------- | ------------ | --------------------------------------------------------------------------------------------------- |
| `configurationId` | `String`   | `""`         | The id of the provided configuration.                                                               |
| `type`            | `String`   | `event.type` | The type of the provided event.                                                                     |
| `payload`         | `Object`   | `{}`         | The payload to be sent. It can be any object. Must be predefined in the process configuration.      |
| `intent`          | `String`   | `undefined`  | Sets the intent field to be set in the payload.                                                     |
| `stopPropagation` | `Boolean`  | `false`      | A boolean which stops the down propagation of the click event to children.                          |
| `onMessage`       | `Function` | `() {}`      | A callback function which triggers the receive event. This is used to handle the received messages. |
| `onOpen`          | `Function` | `() {}`      | A callback function which triggers the open event.                                                  |
| `onError`         | `Function` | `() {}`      | A callback function which triggers the error event.                                                 |
| `onClose`         | `Function` | `() {}`      | A callback function which triggers the close event.                                                 |
| `disabled`        | `Boolean`  | `false`      | A boolean which disables the activity of the OnClick.                                               |

<!-- #### Events
| Event       | Returns  | Note                                                                                                       |
| ----------- | -------- | ---------------------------------------------------------------------------------------------------------- |
| `onMessage` | `string` | A callback function which triggers the receive event. This is used to handle the received messages         |
| `onOpen`    | `Event`  | A callback function which triggers the open event.                                                         |
| `onClose`   | `Event`  | A callback function which triggers the close event.                                                        |
| `onError`   | `Event`  | A callback function which triggers the error event.                                                        |
| `onSend`    | `string` | A callback function which triggers the sending event. This is used to handle the messages about to be sent | | -->

### `OnSubmit`
The OnSubmit component handles all click events passed through its children.

#### Props

| Prop              | Type               | Default       | Note                                                                                                                                                                                                                                                              |
| ----------------- | ------------------ | ------------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `configurationId` | `String`           | `""`          | The id of the provided configuration.                                                                                                                                                                                                                             |
| `type`            | `String`           | `event.type`  | The type of the provided event. As onSubmit can be used for wrapping all kinds of forms, it can be useful to specify the type "utterance" the form is used just to handle input. In this case it must be provided with the first child as `<input type="text" />` |
| `payload`         | `Object`           | `{}`          | The payload to be sent. It can be any object. Must be predefined in the process configuration.                                                                                                                                                                    |
| `intent`          | `String`           | `undefined`   | Sets the intent field to be set in the payload.                                                                                                                                                                                                                   |
| `stopPropagation` | `Boolean`          | `false`       | A boolean which disables the propagation of the OnSubmit capture stop.                                                                                                                                                                                            |
| `onMessage`       | `Function`         | `() {}`       | A callback function which triggers the receive event. This is used to handle the received messages.                                                                                                                                                               |
| `onOpen`          | `Function`         | `() {}`       | A callback function which triggers the open event.                                                                                                                                                                                                                |
| `onError`         | `Function`         | `() {}`       | A callback function which triggers the error event.                                                                                                                                                                                                               |
| `onClose`         | `Function`         | `() {}`       | A callback function which triggers the close event.                                                                                                                                                                                                               |
| `disabled`        | `Boolean`          | `false`       | A boolean which disables the activity of the OnSubmit.                                                                                                                                                                                                            |
| `blacklist`       | `Array.of(String)` | `["submit"]`  | Sets the attribute input types whose names are to be excluded in the payload.                                                                                                                                                                                     |
| `keyType`         | `String`           | `"attribute"` | Sets how to get the key name for the payload in the form.                                                                                                                                                                                                         |
| `customPrefix`    | `String`           | `""`          | Sets the prefix of the custom key name for the payload in the form.                                                                                                                                                                                               |


<!-- #### Events
| Event       | Returns  | Note                                                                                                       |
| ----------- | -------- | ---------------------------------------------------------------------------------------------------------- |
| `onMessage` | `string` | A callback function which triggers the receive event. This is used to handle the received messages         |
| `onOpen`    | `Event`  | A callback function which triggers the open event.                                                         |
| `onClose`   | `Event`  | A callback function which triggers the close event.                                                        |
| `onError`   | `Event`  | A callback function which triggers the error event.                                                        |
| `onSend`    | `string` | A callback function which triggers the sending event. This is used to handle the messages about to be sent | | -->


## License

MIT © [savdav96](https://github.com/savdav96)

