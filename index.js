import OnClick from "./OnClick.vue";
import OnSubmit from "./OnSubmit.vue";
import {WebSocketEventBus} from "./WebSocketEventBus";

module.exports = {
    OnSubmit,
    OnClick,
    WebSocketEventBus
}
